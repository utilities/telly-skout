# SPDX-FileCopyrightText: 2022, 2023, 2024, 2025 Xavier Besnard <xavier.besnard@kde.org>
# Xavier Besnard <xavier.besnard@kde.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: telly-skout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-19 00:45+0000\n"
"PO-Revision-Date: 2025-01-02 09:08+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@kde.org>\n"
"Language-Team: French <French <kde-francophone@kde.org>>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 24.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Steve"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stax@ik.me"

#: main.cpp:60
#, kde-format
msgid "Convergent TV guide based on Kirigami"
msgstr "Guide TV convergent reposant sur Kirigami"

#: main.cpp:63
#, kde-format
msgid "Telly Skout"
msgstr "Telly Skout"

#: main.cpp:67
#, kde-format
msgid "© 2020 KDE Community"
msgstr "© 2020 Communauté KDE"

#: qml/ChannelListDelegate.qml:53
#, kde-format
msgctxt "@info:tooltip"
msgid "Favorite"
msgstr "Favoris"

#: qml/FavoritesPage.qml:19
#, kde-format
msgctxt "@title"
msgid "Favorites"
msgstr "Préférés"

#: qml/FavoritesPage.qml:34
#, kde-format
msgid "Please select favorites"
msgstr "Veuillez sélectionner les favoris"

#: qml/FavoritesPage.qml:39
#, kde-format
msgid "Refetch"
msgstr "Extraire à nouveau"

#: qml/FavoritesPage.qml:145
#, kde-format
msgctxt "placeholder message"
msgid "Information not available"
msgstr "Informations indisponibles"

#: qml/FavoritesPage.qml:205
#, kde-format
msgctxt "@info:tooltip"
msgid "previous day"
msgstr "jour précédent"

#: qml/FavoritesPage.qml:222
#, kde-format
msgctxt "@info:tooltip"
msgid "next day"
msgstr "jour suivant"

#: qml/GroupListDelegate.qml:30
#, kde-format
msgid "Channels"
msgstr "Chaînes"

#: qml/GroupListPage.qml:15
#, kde-format
msgctxt "@title"
msgid "Select Favorites"
msgstr "Veuillez sélectionner les préférés"

#: qml/GroupListPage.qml:24
#, kde-format
msgid "Loading groups..."
msgstr "Chargement des groupes"

#: qml/SelectFavoritesPage.qml:17
#, kde-format
msgctxt "@title"
msgid "Channels"
msgstr "Canaux"

#: qml/SelectFavoritesPage.qml:30 qml/SortFavoritesPage.qml:35
#, kde-format
msgid "Loading channels..."
msgstr "Chargement des chaînes…"

#: qml/SettingsPage.qml:14
#, kde-format
msgctxt "@title"
msgid "Settings"
msgstr "Configuration"

#: qml/SettingsPage.qml:17
#, kde-format
msgctxt "@title:group"
msgid "Appearance"
msgstr "Apparence"

#: qml/SettingsPage.qml:22
#, kde-format
msgctxt "@option:spinbox height of the program"
msgid "Program height:"
msgstr "Hauteur du programme :"

#: qml/SettingsPage.qml:28
#, kde-format
msgctxt "Number in px/min"
msgid "%1px/min"
msgstr "%1 px / min"

#: qml/SettingsPage.qml:38
#, kde-format
msgctxt "@option:spinbox"
msgid "Column width:"
msgstr "Largeur de colonne :"

#: qml/SettingsPage.qml:44 qml/SettingsPage.qml:60
#, kde-format
msgctxt "Number in px"
msgid "%1px"
msgstr "%1 px"

#: qml/SettingsPage.qml:54
#, kde-format
msgctxt "@option:spinbox"
msgid "Font size:"
msgstr "Taille de police :"

#: qml/SettingsPage.qml:68
#, kde-format
msgctxt "@option:check show date selection"
msgid "Show date selection:"
msgstr "Afficher la sélection de date :"

#: qml/SettingsPage.qml:75
#, kde-format
msgctxt "@title:group Programs as TV programs"
msgid "Programs"
msgstr "Programmes"

#: qml/SettingsPage.qml:80
#, kde-format
msgctxt "@option:spinbox"
msgid "Delete old programs after:"
msgstr "Supprimer les anciens programmes après :"

#: qml/SettingsPage.qml:84 qml/SettingsPage.qml:113
#, kde-format
msgctxt "Number in days"
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 jour"
msgstr[1] "%1 jours"

#: qml/SettingsPage.qml:96
#, kde-format
msgid "Fetcher:"
msgstr "Extracteur :"

#: qml/SettingsPage.qml:97
#, kde-format
msgctxt "@option:combobox"
msgid "The fetcher which loads the programs."
msgstr "L'extracteur qui charge les programmes."

#: qml/SettingsPage.qml:108
#, kde-format
msgctxt "@option:spinbox"
msgid "Prefetch:"
msgstr "Précharger :"

#: qml/SettingsPage.qml:125
#, kde-format
msgctxt "@label:textbox"
msgid "File:"
msgstr "Fichier :"

#: qml/SettingsPage.qml:149
#, kde-format
msgctxt "@action:button"
msgid "Open file dialog"
msgstr "Ouvrir la boîte de dialogue de fichiers"

#: qml/SettingsPage.qml:155
#, kde-format
msgid "XML files (*.xml)"
msgstr "Fichier « XML » (*.xml)"

#: qml/SettingsPage.qml:155
#, kde-format
msgid "All files (*)"
msgstr "Tous les fichiers (*)"

#: qml/SortFavoritesPage.qml:16
#, kde-format
msgctxt "@title"
msgid "Sort Favorites"
msgstr "Trier les favoris"

#: qml/TellySkoutGlobalDrawer.qml:17 qml/TellySkoutGlobalDrawer.qml:21
#: qml/TellySkoutGlobalDrawer.qml:29
#, kde-format
msgid "Favorites"
msgstr "Favoris"

#: qml/TellySkoutGlobalDrawer.qml:32 qml/TellySkoutGlobalDrawer.qml:36
#: qml/TellySkoutGlobalDrawer.qml:41
#, kde-format
msgid "Select Favorites"
msgstr "Sélectionnez les favoris"

#: qml/TellySkoutGlobalDrawer.qml:44 qml/TellySkoutGlobalDrawer.qml:48
#: qml/TellySkoutGlobalDrawer.qml:53
#, kde-format
msgid "Sort Favorites"
msgstr "Trier les favoris"

#: qml/TellySkoutGlobalDrawer.qml:56 qml/TellySkoutGlobalDrawer.qml:59
#, kde-format
msgid "Settings"
msgstr "Configuration"

#: qml/TellySkoutGlobalDrawer.qml:62
#, kde-format
msgid "About"
msgstr "À propos"

#: qml/TellySkoutGlobalDrawer.qml:65
#, kde-format
msgid "About Telly Skout"
msgstr "À propos de TellySkout"

#: tvspielfilmfetcher.cpp:30
#, kde-format
msgid "Germany"
msgstr "Allemagne"

#: tvspielfilmfetcher.cpp:300
#, kde-format
msgid "Original title: %1"
msgstr "Titre original : %1"

#: tvspielfilmfetcher.cpp:312
#, kde-format
msgid "Country: %1"
msgstr "Pays : %1"

#: tvspielfilmfetcher.cpp:324
#, kde-format
msgid "Year: %1"
msgstr "Année : %1"

#: tvspielfilmfetcher.cpp:336
#, kde-format
msgid "Duration: %1"
msgstr "Durée : %1"

#: tvspielfilmfetcher.cpp:348
#, kde-format
msgid "FSK: %1"
msgstr "FSK : %1"

#: xmltvfetcher.cpp:43
#, kde-format
msgid "XMLTV"
msgstr "XMLTV"

#~ msgid "UI"
#~ msgstr "UI"

#~ msgid "px"
#~ msgstr "px"

#~ msgid "days"
#~ msgstr "jours"
